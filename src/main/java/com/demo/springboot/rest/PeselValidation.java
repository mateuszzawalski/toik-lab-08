package com.demo.springboot.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PeselValidation {

    private String id;

    public PeselValidation(String id) {
        this.id = id;
    }

    public boolean isValid(){

        if(id.length() != 11){
            return false;
        }else{
            return checkSum();
        }
    }

    private boolean checkSum() {
        int sum = Integer.parseInt(String.valueOf(id.charAt(0))) +
                3 * Integer.parseInt(String.valueOf(id.charAt(1))) +
                7 * Integer.parseInt(String.valueOf(id.charAt(2))) +
                9 * Integer.parseInt(String.valueOf(id.charAt(3))) +
                Integer.parseInt(String.valueOf(id.charAt(4))) +
                3 * Integer.parseInt(String.valueOf(id.charAt(5))) +
                7 * Integer.parseInt(String.valueOf(id.charAt(6))) +
                9 * Integer.parseInt(String.valueOf(id.charAt(7))) +
                Integer.parseInt(String.valueOf(id.charAt(8))) +
                3 * Integer.parseInt(String.valueOf(id.charAt(9)));
        sum %= 10;
        sum = 10 - sum;
        sum %= 10;

        if (sum == Integer.parseInt(String.valueOf(id.charAt(10)))) {
            return true;
        }
        else {
            return false;
        }
    }

}
